const mongoose = require('mongoose');

const CURDSchema = mongoose.Schema({
    name: String,
    quote : String
},{
    timestamps : true
});

module.exports = mongoose.model('quote' , CURDSchema);