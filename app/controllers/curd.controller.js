const Quote = require('../models/curd.model.js');


//Create and Save Data
exports.create = (req , res) => {

    //validate Name field
    if(!req.body.name){
        return res.status(400).send({msg:"Name is required!"});
    }

    //Create Data
    const curd = new Quote({
       name : req.body.name ,
       quote : req.body.quote || "empty!"
    });

    //Save Data into database
    curd.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            msg: err.msg || "Some error occurred while creating the Data."
        });
    });

};

// Retrieve and return all data from the database.
exports.findAll = (req,res) => {

    Quote.find()
        .then(data => {
            res.send(data)
        }).catch(err => {
            res.status(500).send({
                msg: err.msg || "Some error occurred while creating the Data."
            })
        })

};

// Find a single data with a quoteId
exports.findOne = (req,res) => {

    Quote.findById(req.params.quoteId)
        .then(data =>{

            if(!data){
                res.status(404).send({
                    msg: "Data Id not found! " + req.params.quoteId
                });
            }
            res.send(data);

        }).catch(err => {

            if(err.kind === "ObjectId"){
                res.status(404).send({
                    msg: "Data Id not found! " + req.params.quoteId
                });
            }

            return res.status(500).send({
                msg: "Error retrieving data with id " + req.params.quoteId
            });

        })

};

// Update a data identified by the quoteId in the request
exports.update = (req, res) => {

    // Validate Request
    if(!req.body.name) {
        return res.status(400).send({
            msg: "Name field can not be empty"
        });
    }

    // Find note and update it with the request body
    Quote.findByIdAndUpdate(req.params.quoteId, {
        name: req.body.name,
        quote : req.body.quote || "empty!"
    }, {new: true})
        .then(data => {

            if(!data) {
                return res.status(404).send({
                    msg: "Data not found with id " + req.params.quoteId
                });
            }
            res.send(data);

        }).catch(err => {

            if(err.kind === 'ObjectId') {
                return res.status(404).send({
                    msg: "Data not found with id " + req.params.quoteId
                });
            }
            return res.status(500).send({
                msg: "Error updating data with id " + req.params.quoteId
            });

        });

};

// Delete a data with the specified quoteId in the request
exports.delete = (req, res) => {

    Quote.findByIdAndRemove(req.params.quoteId)
        .then(data => {
            if(!data) {
                return res.status(404).send({
                    message: "data not found with id " + req.params.quoteId
                });
            }
            res.send({message: "data deleted successfully!"});
        }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "data not found with id " + req.params.quoteId
            });
        }
        return res.status(500).send({
            message: "Could not delete data with id " + req.params.quoteId
        });
    });
    
};