module.exports = (app) => {

    const data = require('../controllers/curd.controller');

    //Get html
    app.get('/' , (req , res) =>{
       // res.sendFile("../index.html");
        //res.sendFile(path.join(__))
    });

    //Create data
    app.post('/create' , data.create);

    //Retrieve all data
    app.get('/list' , data.findAll);

    //Retrieve single data with data ID
    app.get('/quote/:quoteId' , data.findOne);

    //Update data with data ID
    app.put('/quote/:quoteId' , data.update);

    //Delete data with data ID
    app.delete('/quote/:quoteId' , data.delete);

};