const express = require('express');
const bodyParser = require('body-parser');
const app = express();

//Configuration the database
const dbConfig = require('./config/database.config');
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

// Require Notes routes
require('./app/routes/curd.routes')(app);


// app.get( '/' , ( req , res ) => {
//     res.sendFile(__dirname + "/index.html");
// });



app.listen(3000 ,function () {
    console.log("Server is running on 3000 port.!");
});

// Connecting to the database
mongoose.connect(dbConfig.url ,{useNewUrlParser:true})
    .then(() => {
        console.log("Successfully connected to the database");
    }).catch((err) =>{
        console.log('Could not connect to the database. Exiting now...', err);
        process.exit();
    });

